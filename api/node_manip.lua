
-- Collect nodes with node_timers
minetest.register_on_mods_loaded(function ()
    _holo_deck.cache.node_timers = {}
    for _,node in pairs(minetest.registered_nodes) do
        if node.on_timer then
            _holo_deck.cache.node_timers[node.name] = true
        end
    end
end)

-- Collect node data
--
-- Does NOT remove node afterwards (See holo_deck.get_node)
holo_deck.peek_node = function (pos)
    local node = minetest.get_node_or_nil(pos)
    if node == nil then
        return {name="air", param1=nil, param2=nil}
    end
    if node.name == "ignore" then
        node.name = "air"
    end
    local meta = minetest.get_meta(pos)
    if meta ~= nil then
        -- Convert metadata item stacks to item strings
        for _, invlist in pairs(meta.inventory) do
            for index = 1, #invlist do
                local itemstack = invlist[index]
                if itemstack.to_string then
                    invlist[index] = itemstack:to_string()
                end
            end
        end
        node.meta = meta:to_table()
    end
    if _holo_deck.cache.node_timers[node.name] then
        local timer = minetest.get_node_timer(pos)
        if timer:is_started() then
            node.timer = {
                timeout = timer:get_timeout(),
                elapsed = timer:get_elapsed()
            }
        end
    end
    return node -- {name, param1, param2, (meta), (timer)}
end

-- Collect node data, then removes node
--
-- See holo_deck.peek_node
holo_deck.get_node = function (pos)
    local node = holo_deck.peek_node(pos)
    minetest.remove_node(pos)
    return node
end

-- Sets node via given data
--
-- Given data is expected to be from either holo_deck.get_node or holo_deck.peek_node
holo_deck.set_node = function (pos, data)
    local node = minetest.get_node_or_nil(pos)
    if node == nil then
        node = {name = data.name, param1 = data.param1, param2 = data.param2}
        minetest.set_node(pos, node)
    end
    if data.meta ~= nil then
        local meta = minetest.get_meta(pos)
        meta:from_table(data.meta)
    end
    if data.timer ~= nil then
        local timer = minetest.get_node_timer(pos)
        timer:set(data.timer.timeout, data.timer.elapsed or 0.0)
    end
end
