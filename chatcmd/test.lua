
minetest.register_chatcommand("peek_node", {
    privs = {
        server = true,
    },
    func = function (name, param)
        local pos = minetest.string_to_pos(param) or nil
        if pos then
            local node = holo_deck.peek_node(pos)
            return true, minetest.serialize(node)
        end
    end
})
