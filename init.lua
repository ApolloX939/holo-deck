
holo_deck = {}

_holo_deck = {
    cache = {
        path = {
            mod = minetest.get_modpath("holo_deck"),
            world = minetest.get_worldpath() .. DIR_DELIM .. "holo_deck",
            store = _holo_deck.cache.wp .. DIR_DELIM .. "holo_deck"
        }
    },
    store = minetest.get_mod_storage(),
    log = function (msg)
        if type(msg) == "table" then
            msg = minetest.serialize(msg)
        end
        minetest.log("action", "[holo_deck] " .. tostring(msg))
    end,
    dofile = function (dir, file)
        if file == nil then
            -- dofile(filename)
            dofile(_holo_deck.cache.path.mod .. DIR_DELIM .. dir .. ".lua")
        else
            -- dofile(directory, filename)
            dofile(_holo_deck.cache.path.mod .. DIR_DELIM .. dir .. DIR_DELIM .. file .. ".lua")
        end
    end
}

_holo_deck.dofile("settings") -- settings.lua
_holo_deck.dofile("api", "init") -- api/init.lua
_holo_deck.dofile("chatcmd", "init") -- chatcmd/init.lua
