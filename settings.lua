
if _holo_deck.cache.settings == nil then
    _holo_deck.cache.settings = {
        render_size = 16,
        movement_offset = 2,
        y_placement = 30000
    }
end

local render_size = minetest.settings:get_int("holo_deck.render_size") or nil
if render_size == nil then
    minetest.settings:set_int("holo_deck.render_size", 16)
    _holo_deck.cache.settings.render_size = 16
else
    _holo_deck.cache.settings.render_size = render_size
end

local movement_offset = minetest.settings:get_int("holo_deck.movement_offset") or nil
if movement_offset == nil then
    minetest.settings:set_int("holo_deck.movement_offset", 2)
    _holo_deck.cache.settings.movement_offset = 2
else
    _holo_deck.cache.settings.movement_offset = movement_offset
end

local y_placement = minetest.settings:get_int("holo_deck.y_placement") or nil
if y_placement == nil then
    minetest.settings:set_int("holo_deck.y_placement", 30000)
    _holo_deck.cache.settings.y_placement = 30000
else
    _holo_deck.cache.settings.y_placement = y_placement
end
